import "./components/css/normalize.css";
import "./components/css/app.css";
import NavBar from "./components/widgets/NavBar.jsx";
import { Redirect, Route, Switch } from "react-router-dom";
import HomeLanding from "./components/pages/HomeLanding";
import PackagesLanding from "./components/pages/PackagesLanding.jsx";
import CartLanding from "./components/pages/CartLanding";
import irisGOLanding from "./components/pages/irisGOLanding";
import irisCITYLanding from "./components/pages/irisCITYLanding";
import ownerBenefitsLanding from "./components/pages/ownerBenefitsLanding";
import sectorsLanding from "./components/pages/sectorsLanding";
import regionsLanding from "./components/pages/regionsLanding";
import solutionLanding from "./components/pages/solutionLanding";

function App() {
  return (
    <div className="app">
      <NavBar />
      <Switch>
        <Route path="/home" component={HomeLanding} />
        <Route path="/irisGO" component={irisGOLanding} />
        <Route path="/irisCITY" component={irisCITYLanding} />
        <Route path="/ownerBenefits" component={ownerBenefitsLanding} />
        <Route path="/sectors" component={sectorsLanding} />
        <Route path="/packages" component={PackagesLanding} />
        <Route path="/regions" component={regionsLanding} />
        <Route path="/solution" component={solutionLanding} />
        <Route path="/cart" component={CartLanding} />
        <Redirect from="/" to="/home" />
      </Switch>
    </div>
  );
}

export default App;
