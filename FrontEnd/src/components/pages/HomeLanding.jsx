import React from "react";
import { Link } from "react-router-dom";
import landingimg from "../assets/homelanding.png";
import check from "../assets/check.svg";
import Carousel from "../widgets/Carousel";
function HomeLanding(props) {
  const steps = [
    {
      description:
        "Simply choose your own irisGO solution that is right for your organization",
    },
    {
      description:
        "You can build your own scope of work (SOW) Agreement  to review with associates or counsel.",
    },
    { description: "When completed just save /share " },
    { description: "order online " },
  ];
  return (
    <div className="landing-page">
      <img src={landingimg} className="landing-page-img" />
      <div className="home-landing__top-section">
        <h2>Create Your Own irisGo !</h2>
        <p>In a few simple steps</p>
        <div className="home-landing__top-section__content">
          {steps.map((item, index) => {
            return (
              <div
                className="home-landing__top-section__content-container"
                key={index}
              >
                <div>
                  {index === 3 ? (
                    <img src={check} />
                  ) : (
                    <h5>{`0${index + 1}`}</h5>
                  )}
                </div>

                <p>{item.description}</p>
              </div>
            );
          })}
        </div>
        <Link to="/packages" className="anchor">
          Build Your Own irisGo Now
        </Link>
      </div>
      <Carousel />
    </div>
  );
}

export default HomeLanding;
