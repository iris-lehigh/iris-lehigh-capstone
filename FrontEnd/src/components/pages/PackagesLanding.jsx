import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Card from "../widgets/Card";

/*
API Call code is commented out because I need to fix cors issue with backend
*/


function PackagesLanding(props) {
  const [selctedItem, handleSelectItem] = useState([]);
  // const [packages, setPackages] = useState({});

  const handleSelect = (item) => {
    if (selctedItem.some((e) => e.id === item.id)) {
      selctedItem.splice(selctedItem.indexOf(item), 1);
      handleSelectItem(selctedItem);
    } else handleSelectItem([...selctedItem, item]);
  };

//   useEffect(() => {

//     fetch('https://shrouded-cove-38632.herokuapp.com/packages',
//         {
//             method: "GET",
//             mode: "cors"
//         })
//         .then(response => response.json())
//         .then(response => {
//             try{
//                 console.log(response.json())
//                 setPackages(response.json());
                
//             } catch{
//                 throw Error(response);
//             }
//         })
//         .then(console.log)
//         .catch(console.error)
        
// }, [])

  const items = [
    {
      label: "Raod Assessment",
      description: "some description",
      id: 2,
      src: "images/Tree.jpg",
    },
    {
      label: "Right of way",
      description: "some description",
      id: 3,
      src: "images/Tree.jpg",
    },
    {
      label: "Regulatory",
      description: "some description",
      id: 4,
      src: "images/Tree.jpg",
    },
  ];


  return (
    <div className="packagesLanding">
      <h1>Packages Page</h1>
      <div className="packagesLanding__item-container">
        {items.map((item, key) => (
          <Card key={key} item={item} handleSelect={handleSelect} />
        ))}
      </div>
      <Link
        to={{ pathname: "/cart", state: { selctedItem } }}
        className="packagesLanding__continue-btn"
      >
        Next
      </Link>
    </div>
  );
}

export default PackagesLanding;
