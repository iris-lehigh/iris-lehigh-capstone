import React from "react";
import { useHistory } from "react-router-dom";
function CartLanding(props) {
  const history = useHistory();
  const items = history.location.state.selctedItem;

  return (
    <div className="cartLanding">
      <h2>Your selected {items.length} items</h2>
      {items.map((item, key) => (
        <div key={key} className="cartLanding__item">
          <h3> {item.label}</h3>
          <h4> ${item.price}</h4>
        </div>
      ))}
      <button className="cartLanding__btn">Continue to Check out</button>
    </div>
  );
}

export default CartLanding;
