import React, { useEffect, useState } from "react";
import img1 from "../assets/smartcity.jpg";
import img2 from "../assets/pavement.jpg";
import al from "../assets/arrow-left.svg";
import ar from "../assets/arrow-right.svg";
import am from "../assets/arrow-more.svg";
import { useTransition, animated } from "react-spring";

function Carousel(props) {
  const [index, set] = useState(0);
  const [stop, setstop] = useState(true);

  const rotation = [0, 1, 2, 3, 4];

  useEffect(() => {
    const t =
      stop &&
      setInterval(() => set((state) => (state + 1) % items.length), 4000);
    return () => clearTimeout(t);
  }, [stop]);

  const rotate = useTransition(index, {
    key: index,
    from: { transform: "translateX(100px)", opacity: 0 },
    enter: { transform: "translateX(0)", opacity: 1 },
  });

  const items = [
    {
      label: "Economic Dev & Smart City",
      img: img1,
      p: "Collect data for downtown events including pedestrian flow business metrics. Installed on light poles, the device reports on parked vehicles, traffic volumes and people count",
      link: "#",
    },
    {
      label: "Road & Pavement Assessment",
      img: img2,
      p: "Collect road condition data in real time in real time in accordance to minimum maintenance standards (MMS O. Reg239/02) & Pavement Condition Index (ASTM).",
      link: "#",
    },
    {
      label: "Waste Management",
      img: img1,
      p: "Collects crucial hidden waste issues and participation rates across the municipality to measure diversion programs & campaigns",
      link: "#",
    },
    {
      label: "Transportation & Mobility",
      img: img2,
      p: "Get traffic count and Bike, Pedestrian and Vehicle- type classifications per 20M segments including: traffic monitoring, corridor, and intersection studies all in one pass",
      link: "#",
    },
    {
      label: "Asset Management",
      img: img1,
      p: "Detect & collect Pavement and Right-of -Way imagery using AI to assess and inventory assets such as signs, barriers,  pavement markings, and trees",
      link: "#",
    },
  ];
  return (
    <div className="home-landing__carousel">
      <div className="home-landing__carousel__container">
        <div
          className="home-landing__carousel__container__arrow"
          onClick={() => {
            index > 0 ? set((index - 1) % items.length) : set(items.length - 1);
          }}
        >
          <img src={al} width="35px" height="40px" />
        </div>

        <div className="home-landing__carousel__content">
          {rotate((style, i) => (
            <animated.div style={style}>
              <img style={{ backgroundImage: `url(${items[i].img}` }} />
              <h5>{items[i].label}</h5>
              <p>{items[i].p}</p>
              <a href={items[i].link}>
                <span>Learn More</span>
                <img src={am} />
              </a>
            </animated.div>
          ))}
        </div>

        <div
          className="home-landing__carousel__container__arrow"
          onClick={() => {
            set((index + 1) % items.length);
          }}
        >
          <img src={ar} width="35px" height="40px" />
        </div>
      </div>
      <div className="home-landing__carousel__selections">
        {rotation.map((item, key) => (
          <li
            key={key}
            className={`home-landing__carousel__selections__list ${
              index === item ? "active" : ""
            }`}
            onMouseEnter={() => {
              set(item);
              setstop(false);
            }}
            onMouseLeave={() => {
              setstop(true);
            }}
          >
            {key + 1}
          </li>
        ))}
      </div>
    </div>
  );
}

export default Carousel;
