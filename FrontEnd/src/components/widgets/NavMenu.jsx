import React, { useState } from "react";
import menu from "../assets/menu.svg";
import closedmenu from "../assets/closedmenu.svg";
import { Link } from "react-router-dom";
function NavMenu() {
  //click determines if the left menu has been clicked
  const [click, setClick] = useState(false);
  //this is the function that switches the value of click
  const handleClick = () => setClick(!click);

  const links = [
    { label: "irisGO", path: "/irisGO" },
    { label: "irisCITY", path: "/irisCITY" },
    { label: "Owner Benefits", path: "/ownerBenefits" },
    { label: "Sectors", path: "/sectors" },
    { label: "Regions", path: "/regions" },
    { label: "Solution", path: "/solution" },
    { label: "Start Building Now", path: "/packages" },
  ];
  /*lol*/

  return (
    <>
      <img
        src={click ? closedmenu : menu}
        onClick={handleClick}
        className="nav-menu-icon"
      />
      <ul className={click ? "nav-menu active" : "nav-menu"}>
        {links.map((item, key) => (
          <li className="nav-item" key={key}>
            <Link
              to={item.path}
              className="nav-links"
              onClick={() => setClick(false)}
            >
              {item.label}
            </Link>
          </li>
        ))}
      </ul>
    </>
  );
}

export default NavMenu;
