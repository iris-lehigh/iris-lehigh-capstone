import React, { useState } from "react";

function Card({ item, handleSelect }) {
  const [select, setSelect] = useState(false);

  return (
    <div
      className={`card ${select ? "selected" : ""}`}
      onClick={() => {
        handleSelect(item);
        setSelect(!select);
      }}
    >
      <img src={item.src} />
      <h5>{item.label}</h5>
      <p>{item.description}</p>
    </div>
  );
}

export default Card;
