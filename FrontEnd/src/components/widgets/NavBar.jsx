import React from "react";
import { Link, NavLink } from "react-router-dom";
import cart from "../assets/cart.svg";
import irisGO from "../assets/irisGO.svg";
import NavMenu from "./NavMenu";
function Navbar(props) {
  return (
    <nav className="navbar">
      <div className="navbar__side">
        <Link to="#">How it works</Link>
        <Link to="#">About irisGo</Link>
        <Link to="#">Contact Us</Link>
      </div>

      <div className="navbar__main">
        <div>
          <NavMenu />
          <img
            src={irisGO}
            onClick={() => {
              window.location = "/home";
            }}
          />
        </div>
        <div className="navbar__main__nav-items-container">
          <NavLink to="/home">Home</NavLink>
          <NavLink to="/packages">Packages</NavLink>
          <NavLink to="/account">Account</NavLink>
          <Link to={{ pathname: "/cart", state: { selctedItem: [] } }}>
            <img src={cart} />
          </Link>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
